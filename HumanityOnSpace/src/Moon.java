import abtractclasses.Colony;

public class Moon extends Colony {

    private final String colonyName = "Moon Colony";
    private String colonyMayor;
    private int population;

    public Moon(String colonyMayor, int population) {
        this.colonyMayor = colonyMayor;
        this.population = population;
    }

    void rotateAroundTheSun() {
        System.out.println("Moon rotate around the Sun");
    }

    void rotateAroundYourSelf() {
        System.out.println("Moon rotate around its-self");
    }

    void earthquake(double x, double y) {
        System.out.println("Earthquake at Moon at " + x + "," + y);
    }

    void declareWar(Colony colony){
        System.out.println("Moon declares war at" + colony.colonyName);
    }

    void makePeace(Colony colony){
        System.out.println("Moon made peace with " + colony.colonyName);
    }

    void exploreTheUniverse(){
        System.out.println("Moon explores universe!");
    }

    void launchSatellite(){
        ManMadeSatellite mms = new ManMadeSatellite();

    }

    public String getColonyName() {
        return colonyName;
    }

    public String getColonyMayor() {
        return colonyMayor;
    }

    public void setColonyMayor(String colonyMayor) {
        this.colonyMayor = colonyMayor;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "Moon{" +
                "colonyName='" + colonyName + '\'' +
                ", colonyMayor='" + colonyMayor + '\'' +
                ", population=" + population +
                '}';
    }

}
