package interfaces;

import abtractclasses.Planet;

public interface Floatable {

    void floatAroundParent(Planet planet);

    void changeDistance(Planet planet);

}
