package abtractclasses;

public abstract class Planet {

    private String planetName;
    private String planetDiameter;
    private double planetPercentageWater;
    private double planetIncrementSun;

    void rotateAroundTheSun() {
    }

    void rotateAroundYourSelf() {
    }

    void earthquake(double x, double y) {
    }

    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    public String getPlanetDiameter() {
        return planetDiameter;
    }

    public void setPlanetDiameter(String planetDiameter) {
        this.planetDiameter = planetDiameter;
    }

    public double getPlanetPercentageWater() {
        return planetPercentageWater;
    }

    public void setPlanetPercentageWater(double planetPercentageWater) {
        this.planetPercentageWater = planetPercentageWater;
    }

    public double getPlanetIncrementSun() {
        return planetIncrementSun;
    }

    public void setPlanetIncrementSun(double planetIncrementSun) {
        this.planetIncrementSun = planetIncrementSun;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "planetName='" + planetName + '\'' +
                ", planetDiameter='" + planetDiameter + '\'' +
                ", planetPercentageWater=" + planetPercentageWater +
                ", planetIncrementSun=" + planetIncrementSun +
                '}';
    }

}
