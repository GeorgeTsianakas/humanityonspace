package abtractclasses;

public abstract class Colony extends Planet {

    public String colonyName;
    public String colonyMayor;
    public int population;

    void declareWar() {
    }

    void makePeace() {
    }

    void exploreTheUniverse() {
    }

    void launchSatellite() {
    }

}
