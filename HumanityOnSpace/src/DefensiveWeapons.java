import abtractclasses.Planet;
import interfaces.Floatable;

import java.util.Date;

public class DefensiveWeapons extends SpaceTech implements Floatable {

    private int id;
    private boolean flagIfNuclear;
    private int numberOfRockets;

    public DefensiveWeapons(Date yearOfCreation, String creatorName, int id, boolean flagIfNuclear, int numberOfRockets) {
        super(yearOfCreation, creatorName);
        this.id = id;
        this.flagIfNuclear = flagIfNuclear;
        this.numberOfRockets = numberOfRockets;
    }

    @Override
    public void floatAroundParent(Planet planet) {
        System.out.println("Floats around " + planet.getPlanetName());
    }

    @Override
    public void changeDistance(Planet planet) {
        System.out.println("Distance from " + planet.getPlanetName() + "= //TODO");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFlagIfNuclear() {
        return flagIfNuclear;
    }

    public void setFlagIfNuclear(boolean flagIfNuclear) {
        this.flagIfNuclear = flagIfNuclear;
    }

    public int getNumberOfRockets() {
        return numberOfRockets;
    }

    public void setNumberOfRockets(int numberOfRockets) {
        this.numberOfRockets = numberOfRockets;
    }

    @Override
    public String toString() {
        return "DefensiveWeapons{" +
                "id=" + id +
                ", flagIfNuclear=" + flagIfNuclear +
                ", numberOfRockets=" + numberOfRockets +
                '}';
    }

}
