import abtractclasses.Planet;
import interfaces.Floatable;

public class SatellitePlanets extends Planet implements Floatable {

    private String planetName;
    private String planetDiameter;
    private double planetPercentageWater;
    private double planetIncrementSun;

    public SatellitePlanets(String planetName, String planetDiameter, double planetPercentageWater, double planetIncrementSun) {
        this.planetName = planetName;
        this.planetDiameter = planetDiameter;
        this.planetPercentageWater = planetPercentageWater;
        this.planetIncrementSun = planetIncrementSun;
    }

    @Override
    public void floatAroundParent(Planet planet) {
        System.out.println("Floats around " + planet.getPlanetName());
    }

    @Override
    public void changeDistance(Planet planet) {
        System.out.println("Distance from " + planet.getPlanetName() + "= //TODO");
    }

    @Override
    public String getPlanetName() {
        return planetName;
    }

    @Override
    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    @Override
    public String getPlanetDiameter() {
        return planetDiameter;
    }

    @Override
    public void setPlanetDiameter(String planetDiameter) {
        this.planetDiameter = planetDiameter;
    }

    @Override
    public double getPlanetPercentageWater() {
        return planetPercentageWater;
    }

    @Override
    public void setPlanetPercentageWater(double planetPercentageWater) {
        this.planetPercentageWater = planetPercentageWater;
    }

    @Override
    public double getPlanetIncrementSun() {
        return planetIncrementSun;
    }

    @Override
    public void setPlanetIncrementSun(double planetIncrementSun) {
        this.planetIncrementSun = planetIncrementSun;
    }

    @Override
    public String toString() {
        return "SatellitePlanets{" +
                "planetName='" + planetName + '\'' +
                ", planetDiameter='" + planetDiameter + '\'' +
                ", planetPercentageWater=" + planetPercentageWater +
                ", planetIncrementSun=" + planetIncrementSun +
                '}';
    }

}
