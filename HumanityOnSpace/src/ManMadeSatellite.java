import abtractclasses.Planet;
import interfaces.Floatable;

public class ManMadeSatellite implements Floatable {

    private String name;
    private String purpose;
    private int numberOfPeople;

    public ManMadeSatellite() {
    }

    public ManMadeSatellite(String name, String purpose, int numberOfPeople) {
        this.name = name;
        this.purpose = purpose;
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public void floatAroundParent(Planet planet) {
        System.out.println("Floats around " + planet.getPlanetName());
    }

    @Override
    public void changeDistance(Planet planet) {
        System.out.println("Distance from " + planet.getPlanetName() + "= //TODO");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public String toString() {
        return "ManMadeSatellite{" +
                "name='" + name + '\'' +
                ", purpose='" + purpose + '\'' +
                ", numberOfPeople=" + numberOfPeople +
                '}';
    }

}
