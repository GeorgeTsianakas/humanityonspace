import java.util.Date;

public class SpaceTech {

    private  Date yearOfCreation;
    private  String creatorName;

    public SpaceTech() {
    }

    public SpaceTech(Date yearOfCreation, String creatorName) {
        this.yearOfCreation = yearOfCreation;
        this.creatorName = creatorName;
    }

    public void selfDestruct() {
        System.out.println("Self-Destruct");
    }

    public Date getYearOfCreation() {
        return yearOfCreation;
    }

    public String getCreatorName() {
        return creatorName;
    }

    @Override
    public String toString() {
        return "SpaceTech{" +
                "yearOfCreation=" + yearOfCreation +
                ", creatorName='" + creatorName + '\'' +
                '}';
    }

}
