import abtractclasses.Planet;
import interfaces.Floatable;

public class Debris implements Floatable {

    private int id;
    private String material;

    public Debris(int id, String material) {
        this.id = id;
        this.material = material;
    }

    @Override
    public void floatAroundParent(Planet planet) {
        System.out.println("Floats around " + planet.getPlanetName());
    }

    @Override
    public void changeDistance(Planet planet) {
        System.out.println("Distance from " + planet.getPlanetName() + "= //TODO");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Debris{" +
                "id=" + id +
                ", material='" + material + '\'' +
                '}';
    }

}
