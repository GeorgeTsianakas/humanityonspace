import abtractclasses.Planet;

public class Earth extends Planet {

    private final String planetName = "Earth";
    private final String planetDiameter;
    private final double planetPercentageWater;
    private final double planetIncrementSun;

    public Earth(String planetDiameter, double planetPercentageWater, double planetIncrementSun) {
        this.planetDiameter = planetDiameter;
        this.planetPercentageWater = planetPercentageWater;
        this.planetIncrementSun = planetIncrementSun;
    }

    void rotateAroundTheSun(){
        System.out.println("Earth rotate around the Sun");
    }

    void rotateAroundYourSelf(){
        System.out.println("Earth rotates around herself");
    }

    void earthquake(double x,double y){
        System.out.println("Earthquake at " + x + "," + y);
    }

    public String getPlanetName() {
        return planetName;
    }

    public String getPlanetDiameter() {
        return planetDiameter;
    }

    public double getPlanetPercentageWater() {
        return planetPercentageWater;
    }

    public double getPlanetIncrementSun() {
        return planetIncrementSun;
    }

    @Override
    public String toString() {
        return "Earth{" +
                "planetName='" + planetName + '\'' +
                ", planetDiameter='" + planetDiameter + '\'' +
                ", planetPercentageWater=" + planetPercentageWater +
                ", planetIncrementSun=" + planetIncrementSun +
                '}';
    }

}
